function [E] = VehicleModelBasic(speedProfile, accelerationProfile, Parameter)
% Calculate Energye Consumption for given speed and acceleration profile
% Input:  speed        = cell array with speedprofile 1/s [km/h]
%         acceleration = cell array with acceleration profile 1/s [m/s2]
%         Parameter    = Vehicle parameter from database
% Output: E            = Consumed energy for driving profile [kWh]

%% Calculation

% number of profiles
nProfile = length(speedProfile);

E = zeros(nProfile, 1);

for iProfile = 1:nProfile
    
    speed = speedProfile(iProfile);
    acceleration = accelerationProfile(iProfile);

    % convert speed from km/h to m/s
    speed = speed / 3.6;
    
    % total vehicle mass [kg]
    mTotal = Parameter.massVehicle;
    
    % efficiency battery -> wheel
    efficiencyBattWheel = ...
        Parameter.efficiencyTransmission * Parameter.efficiencyMotor * Parameter.efficiencyInverter;
    
    % driving resistance [kW]
    Pres = ...
        (Parameter.A + Parameter.B .* speed + Parameter.C .* speed.^2) .* ...
        speed / ...
        1E3;
    % acceleration resistance [kW]
    Pacc = mTotal * Parameter.massFactor * acceleration .* speed / 1E3;
    % driving power [kW]
    Pdrive = Pres + Pacc;
    % board net power [kW]
    Pboard = ones(size(speed)) * (Parameter.powerAuxiliary / Parameter.efficiencyConverter ...
        + Parameter.powerAircon);
    % balance power [kW]
    Pbalance = efficiencyBattWheel * Pdrive + Pboard;
    
    % get driving modes
    drive = Pdrive >= 0;
    regenerateLow = (~drive & Pbalance >= 0);
    regenerateHigh = (~drive & Pbalance < 0);
    
    % calculate battery power [kW]
    Pbatt = NaN(size(speed));
    Pbatt(drive) = ...
        (Pdrive(drive) / efficiencyBattWheel + Pboard(drive) / Parameter.efficiencyInverter) / ...
        Parameter.efficiencyBattery ^ 0.5;
    Pbatt(regenerateLow) = ...
        (efficiencyBattWheel * Pdrive(regenerateLow) + ...
        Pboard(regenerateLow) / Parameter.efficiencyInverter) / ...
        Parameter.efficiencyBattery ^ 0.5;
    Pbatt(regenerateHigh) = ...
        (efficiencyBattWheel * Pdrive(regenerateHigh) + ...
        Pboard(regenerateHigh) / Parameter.efficiencyInverter) * ...
        Parameter.efficiencyBattery ^ 0.5;
    
    % calculate energy demand of whole trip [kWh]
    E(iProfile) = sum(Pbatt) / 3600;
end

