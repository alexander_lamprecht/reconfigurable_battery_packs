import csv

class Vehicle(object):
    """docstring"""

    name = 'none'
    massVehicle = 0.0
    massFactor = 0.0
    A = 0.0
    B = 0.0
    C = 0.0
    batteryCapacity = 0.0
    usableCapacityFastCharging = 0.0
    usableCapacitySlowCharging = 0.0
    maxChargingPowerDC = 0.0
    maxChargingPowerAC = 0.0
    efficiencyBattery = 0.0
    efficiencyInverter = 0.0
    efficiencyConverter = 0.0
    efficiencyMotor = 0.0
    efficiencyTransmission = 0.0
    efficiencyOnboardCharger = 0.0
    powerAuxiliary = 0.0
    powerAircon = 0.0
    correctionDistance = 0.0
    correctionEnergy = 0.0

    def __init__(self,idSimulationVehicle):
        self.idSimulationVehicle = idSimulationVehicle


    def load_csv_parameters(self, file_path):
        with open(file_path) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            next(readCSV)
            for row in readCSV:
                if int(row[0]) == self.idSimulationVehicle:
                    self.name = row[1]
                    self.massVehicle = float(row[2])
                    self.massFactor = float(row[3])
                    self.A = float(row[4])
                    self.B = float(row[5])
                    self.C = float(row[6])
                    self.batteryCapacity = float(row[7])
                    self.usableCapacityFastCharging = float(row[8])
                    self.usableCapacitySlowCharging = float(row[9])
                    self.maxChargingPowerDC = float(row[10])
                    self.maxChargingPowerAC = float(row[11])
                    self.efficiencyBattery = float(row[12])
                    self.efficiencyInverter = float(row[13])
                    self.efficiencyConverter = float(row[14])
                    self.efficiencyMotor = float(row[15])
                    self.efficiencyTransmission = float(row[16])
                    self.efficiencyOnboardCharger = float(row[17])
                    self.powerAuxiliary = float(row[18])
                    self.powerAircon = float(row[19])
                    self.correctionDistance = float(row[20])
                    self.correctionEnergy = float(row[21])

        print('Parameters for',self.name,'loaded')