import matplotlib.pyplot as plt
import datetime
import numpy as np
from BatteryPack import BatteryPack
import csv


carID = 10
# cycle = 'WLTP_class_3'
cycle = 'Linear_acc'
cycle_file_path = cycle + '.csv'

cells_s = 96

bat_pack_size = 324e3
cells_p = 4
R_S = 0.35e-3
R_plug = 0.5e-3
ch_rate = 3
S_p = 7


bat_pack_size_array = range(10,100,5)
cells_p_array = range(1, 11, 1)
R_S_array = np.arange(0.1e-3,1.05e-3,0.05e-3)
R_plug_array = np.arange(0.05e-3,1.05e-3,0.05e-3)
ch_rate_array= np.arange(0.5,5.5,0.5)
S_p_array = range(1,11,1)

config_array = [False, True]

Loss_energy_array = []
datestr = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M")
subfolder = 'results/'
var_param = 'cells_p'
doplot=True

file_name = subfolder+datestr+'_variable_'+var_param+'.csv'
            # 'bat_pack_size_'+ str(bat_pack_size/1e3)+'_cells_in_series_'+str(cells_in_series)+'_ch_rate_'+str(ch_rate)+'_'+str(cycle)+'.csv'
with open(file_name, 'w') as csvfile:
    fieldnames = ['Pack_Size', 'Reconfigurable','Pack_configuration', 'Charging_rate', 'R_plug', 'R_S','S_p', 'Losses_charging','Losses_discharging','Losses_total']
    writer = csv.DictWriter(csvfile, lineterminator='\n', fieldnames=fieldnames)
    writer.writeheader()
    for config in config_array:
        for cells_p in cells_p_array:
        # for bat_pack_size in bat_pack_size_array:
        # for R_S in R_S_array:
        # for R_plug in R_plug_array:
        # for ch_rate in ch_rate_array:
        # for S_p in S_p_array:
            bat_pack = []
            pack_config = str(cells_s)+ 'S'+ str(cells_p)+ 'P'
            bat_pack = BatteryPack(cells_p, cells_s, bat_pack_size, 'charging', config, config, 0)
            print('Cell configuration: ',pack_config, '; Initial SOC: ',bat_pack.SOC,'%')
            bat_pack.ch_rate = ch_rate
            bat_pack.R_S = R_S
            bat_pack.R_plug = R_plug
            bat_pack.S_p = S_p


            bat_pack.update_pack()
            losses_ch = bat_pack.calc_charge_losses(100)

            print('Losses ch:', round(losses_ch, 2),"SOC:",round(bat_pack.SOC,1),'%')
            # bat_pack.config = False
            bat_pack.status = 'driving'
            bat_pack.update_pack()
            losses_disch = bat_pack.calc_cycle_losses(cycle_file_path,carID,100)
            # losses_disch=0
            print('Losses disch:', round(losses_disch, 2), "SOC:", round(bat_pack.SOC, 1),"%; Distance travelled:", round(bat_pack.dist_travelled/1000,2),'km')
            print('Loss Energy:',round((losses_ch+losses_disch)/1000, 2), 'kWh')
            print()
            writer.writerow({'Pack_Size':bat_pack_size, 'Reconfigurable': config,  'Losses_charging': losses_ch, 'Charging_rate': str(ch_rate)+'C', 'R_plug':R_plug, 'R_S':R_S,'S_p':S_p, 'Pack_configuration': pack_config, 'Losses_discharging': losses_disch, 'Losses_total': losses_ch+losses_disch})
            Loss_energy_array.append(losses_ch+losses_disch)

if doplot==True:
    plt.figure(1)
    half = round(len(Loss_energy_array)/2)
    plt.plot(cells_p_array, [x for x in Loss_energy_array[:half]])
    plt.plot(cells_p_array, [x for x in Loss_energy_array[half:]])
    plt.title('Loss Energy for ' + bat_pack.vehicle.name + ', ' + str(bat_pack.pack_cap/1e3) +'kWh, ' + str(ch_rate) + 'C, ' + cycle)
    plt.legend(['not Reconfigurated', 'Reconfigurated'])
    plt.xlabel('# of cells in parallel')
    plt.xlim([min(cells_p_array), max(cells_p_array)])
    plt.ylabel('Loss Energy in Wh')
    plt.show()