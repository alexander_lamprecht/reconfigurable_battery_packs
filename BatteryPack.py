import csv
from Vehicle import Vehicle

class BatteryPack(object):
    """docstring for BatteryPack"""

    V_OC = 3.6              # cell discharging Voltage
    R_plug = .5e-3          # resistance of the plug
    R_S = 0
    R_S1 = 0          # Switch 1 Resistance
    R_S2 = 0          # Switch 2 Resistance
    R_S3 = 0          # Switch 3 Resistance
    S_p = 7                 # Switches in parallel
    R_i_init = 22.15e-3         # Cell inner resistance
    R_i = 0

    V_C = 0                 # in V
    V_OC_pack = 0           # Open circuit voltage of the pack in V
    V_bp = 0                # in V
    cell_cap = 0            # in Ah
    I_C = 0                 # charging/discharging current
    num_cells = 0           # total number of cells
    ch_rate = 0             # Charging/discharging rate C; positive value for charging negative value for discharging
    vehicle = []

    R_eq = 0
    dist_travelled = 0

    class DriveCycle(object):
        n_cycle = 0
        dist_travel = 0
        time = []
        speed = []
        acc = []
        P = []

    def __init__(self, num_p, num_s, pack_cap, status, is_reconfigurable, reconfig, SOC):
        self.num_p = num_p                          # number of cells parallel
        self.num_s = num_s                          # number of cells in series
        self.pack_cap = pack_cap                    # Total battery capacity in Wh
        self.status = status                        # charging status ['charging' / 'discharging']
        self.is_reconfigurable = is_reconfigurable  # choose if configuration has switches or not [True / False]
        self.reconfig = reconfig                    # reconfigured or not [True / False]
        self.SOC = SOC                              # State of Charge
        self.R_S1 = self.R_S / self.S_p
        self.R_S2 = self.R_S / self.S_p
        self.R_S3 = self.R_S / self.S_p


    def update_pack(self):
        self.R_S1 = self.R_S / self.S_p
        self.R_S2 = self.R_S / self.S_p
        self.R_S3 = self.R_S / self.S_p
        self.num_cells = self.num_p * self.num_s
        # print(self.num_cells)

        self.cell_cap = self.pack_cap / self.num_cells / self.V_OC
        self.R_i = self.R_i_init / (self.cell_cap / 2.5)
        # print(self.cell_cap)
        self.V_OC_pack = self.num_s * self.V_OC

        # if not self.is_reconfigurable:
        #     self.R_S1 = 0
        #     self.R_S2 = 0
        #     self.R_S3 = 0

        if self.status == 'charging':
            if self.is_reconfigurable:
                if self.reconfig:
                    self.V_OC_pack = self.num_cells * self.V_OC
                    self.I_C = self.pack_cap * self.ch_rate / self.V_OC / self.num_cells
                    self.R_eq = self.num_cells * self.R_i + (self.num_cells - 1) * self.R_S3 + self.R_S2 # (self.num_cells - 1) * self.R_S3 + self.R_S2 wrong!!!!!!!
                    self.V_bp = self.I_C * self.R_eq + self.num_cells * self.V_OC
                else:
                    self.I_C = self.num_p * self.cell_cap * self.ch_rate
                    self.R_eq = (self.R_S1 + self.R_S2 + self.R_i) / self.num_p * self.num_s
                    self.V_bp = self.V_OC * self.num_s + self.R_eq * self.I_C
                # self.V_C = self.V_bp + self.I_C * 2 * self.R_plug
            else:
                self.I_C = self.num_p * self.cell_cap * self.ch_rate
                self.R_eq = self.R_i / self.num_p * self.num_s
                self.V_bp = self.V_OC * self.num_s + self.R_eq * self.I_C
            self.V_C = self.V_bp + self.I_C * 2 * self.R_plug



        elif self.status == 'driving':
            self.I_C = self.num_p * self.cell_cap * self.ch_rate
            if self.is_reconfigurable:
                self.R_eq = (self.R_S1 + self.R_S2 + self.R_i) / self.num_p * self.num_s
            else:
                self.R_eq = self.R_i / self.num_p * self.num_s
            self.V_bp = self.V_OC * self.num_s + self.R_eq * self.I_C
            self.V_C = self.V_bp
        else:
            self.status == 'idle'


    def calc_loss_power(self):
        if self.status == 'charging':
            return self.I_C ** 2 * (self.R_eq + 2 * self.R_plug)
        elif self.status == 'driving':
            return self.I_C ** 2 * self.R_eq

    def calc_loss_energy(self, dura):
        return self.calc_loss_power() * dura

    def calc_energy_throughput(self, dura):
        return abs(self.V_C * self.I_C * dura)

    def load_cycle(self, cycle, file_path):
        with open(file_path) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            next(readCSV)
            next(readCSV)
            next(readCSV)
            cycle.time = []
            cycle.speed = []
            cycle.acc = []
            for row in readCSV:
                cycle.time.append(float(row[1]))
                cycle.speed.append(float(row[3]))
                cycle.acc.append(float(row[4]))
            cycle.n_cycle = len(cycle.time)
        return cycle


    def calc_P_cycle(self, cycle, idSimulationVehicle):
        self.vehicle = Vehicle(idSimulationVehicle)
        self.vehicle.load_csv_parameters("vehicle_energy_model/vehicleParameter.csv")
        cycle.P = []
        for iCycle in range(cycle.n_cycle):
            cycle.dist_travel += cycle.speed[iCycle]/3.6
            speed = cycle.speed[iCycle]
            acc = cycle.acc[iCycle]
            # convert speed from km/h to m/s
            speed /= 3.6
            # efficiency battery -> wheel
            efficiencyBattWheel = self.vehicle.efficiencyTransmission * self.vehicle.efficiencyMotor * self.vehicle.efficiencyInverter
            # driving resistance [W]
            pRes = (self.vehicle.A + self.vehicle.B * speed + self.vehicle.C * speed ** 2) * speed
            ## source to understand paramerter http://collaboratory.ucr.edu/files/Diesel_Vehicle_Fuel_Consumption.pdf
            # acceleration resistance [W]
            pAcc = self.vehicle.massVehicle * self.vehicle.massFactor * acc * speed
            # driving power [W]
            pDrive = pRes + pAcc
            # board net power [W]
            pBoard = 1e3 * self.vehicle.powerAuxiliary/self.vehicle.efficiencyConverter + 1e3 * self.vehicle.powerAircon
            # balance Power [W]
            pBalance = efficiencyBattWheel * pDrive + pBoard
            pBatt = 0
            # battery power [W]
            if pDrive >= 0:
                pBatt = (pDrive / efficiencyBattWheel + pBoard / self.vehicle.efficiencyInverter) / self.vehicle.efficiencyBattery**0.5
            else:
                if pBalance >= 0:
                    pBatt = (efficiencyBattWheel * pDrive + pBoard / self.vehicle.efficiencyInverter) / self.vehicle.efficiencyBattery ** 0.5
                else:
                    pBatt = (efficiencyBattWheel * pDrive + pBoard / self.vehicle.efficiencyInverter) * self.vehicle.efficiencyBattery ** 0.5
            # Energy demand for one time step
            cycle.P.append(pBatt)
        return cycle


    def calc_cycle_losses(self, file_path,idSimulationVehicle, DOD):
        losses = 0
        dist = 0
        cycle = self.DriveCycle()
        cycle = self.calc_P_cycle(self.load_cycle(cycle,file_path),idSimulationVehicle)
        # self.load_cycle(file_path)
        self.update_pack()
        initSOC = self.SOC
        while initSOC - DOD < self.SOC and self.SOC > 0:
            i=0
            for p in cycle.P:
                self.ch_rate = p / self.pack_cap
                # print(round(self.V_C,2),round(self.I_C,2))

                self.update_pack()
                losses += self.calc_loss_energy(1) / 3600 #calculates the losses and adds them up

                self.SOC -= 100 * self.I_C * self.V_C / 3600 / self.pack_cap
                if self.SOC <= 0:
                    return losses
                self.dist_travelled += cycle.speed[i]/3.6
                i +=1
        return losses


    def calc_charge_losses(self, DOC):
        losses = 0
        initSOC = self.SOC
        while self.SOC <= initSOC + DOC and self.SOC < 100:
            self.update_pack()
            self.SOC += 100 * self.I_C * self.V_C / 3600 / self.pack_cap
            losses += self.calc_loss_energy(1) / 3600
        print('Charging current:',self.I_C)
        return losses