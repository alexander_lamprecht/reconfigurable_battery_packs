'''
Created on 10 Aug, 2016

@author: swaminathan.narayana
'''

import math
import numpy as np
import scipy as s
import matplotlib
from matplotlib import cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.pyplot import*


class params(object):

    def __init__(self):
        #         self.Capacity = [20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,210,220,230,240,250]  # Total capacity of the battery pack (120 Ah)
        self.Capacity = [40, 80, 120, 160, 200, 250]  # Total capacity of the battery pack (120 Ah)
        self.Rmos = 0.4 * 1e-3
        self.Rcontact = 0.5 * 1e-3
        self.Rcable = 3 * 1e-3
        self.Rcell = 1 * 1e-3


def Calculate_powerLoss(I, R):
    Current = I
    Resistance = R

    Ploss = Current**2 * Resistance

    return Ploss


if __name__ == '__main__':

    BatteryPack = params()

    Np = [i for i in range(2, 10)]
    #Nr = [j for j in range(3,8)]
    pmos = 3

    TotalCells = 108

    TotalResistance = (BatteryPack.Rcontact + BatteryPack.Rcable) * 2 + (BatteryPack.Rcell) * TotalCells

#     Ich_noreconfig = 3 * BatteryPack.Capacity
#
#     powerLoss_noreconfig = Calculate_powerLoss(I=Ich_noreconfig, R=TotalResistance)
#
#     Eloss_noreconfig = powerLoss_noreconfig * 0.25 #15 mins of fast charging

    plt.figure()
    axes = plt.gca()
    # axes.set_ylim([0,500])
    axes.set_xlabel('No of cells in Parallel')
    axes.set_ylabel('Energy saving in [Wh]')
    # axes.set_xticks([1,2,4,6,8,10,12,14,16,18,20])

    for Cap in BatteryPack.Capacity:

        Ich_noreconfig = 3 * Cap

        powerLoss_noreconfig = Calculate_powerLoss(I=Ich_noreconfig, R=TotalResistance)

        Eloss_noreconfig = powerLoss_noreconfig * 0.25  # 15 mins of fast charging

        Esavings = []
        ElossCharge = []
        ElossDischarge = []
        Eloss_reconfig = []

        for n in Np:

            Icharge = (Cap / n) * 3

            RmoschargeTotal = (BatteryPack.Rmos / pmos) * n * TotalCells + (BatteryPack.Rcell * n * TotalCells)

            Rtotal = RmoschargeTotal + (BatteryPack.Rcontact + BatteryPack.Rcable) * 2

            powerLoss = Calculate_powerLoss(I=Icharge, R=Rtotal)

            chargeEnergyLoss = powerLoss * 0.25

            Idischarge = 0.1 * Cap / (n)

            RmosDischTotal = (BatteryPack.Rmos / pmos) * 2

            singleCellLoss = Calculate_powerLoss(I=Idischarge, R=RmosDischTotal)

            dischargeEnergyLoss = singleCellLoss * n * TotalCells * 48  # 2 days of driving

            TotalEnergyLoss = dischargeEnergyLoss + chargeEnergyLoss

            energySavings = Eloss_noreconfig - TotalEnergyLoss

            Eloss_reconfig.append(TotalEnergyLoss)
            Esavings.append(energySavings)
            ElossCharge.append(chargeEnergyLoss)
            ElossDischarge.append(dischargeEnergyLoss)

        #CS = plt.plot(Np, Ploss)

        CS = plt.plot(Np, Esavings, label=Cap)
        #CS = plt.plot(Np, Eloss_reconfig, label=pmos)
#        CS1 = plt.plot(Np,ElossCharge)
#        CS2 = plt.plot(Np,ElossDischarge)

    Ploss_noreconfigList = []
    for count in Np:
        Ploss_noreconfigList.append(Eloss_noreconfig)

    #CS = plt.plot(Np, Ploss_noreconfigList)

    plt.legend(loc='lower right')
    plt.show()
